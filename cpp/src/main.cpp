//#include <doctest/doctest.h>

#include <string>
#include <iostream>

#include "parser.hpp"
#include "node.hpp"

using namespace dicey;

void print_tree(node* root, const std::string& prefix = "", bool is_left = true) {
    if (root == nullptr) {
        return;
    }
    
    std::cout << prefix;
    std::cout << (is_left ? "├── " : "└── ");
    std::cout << root->get_value() << std::endl;

    if (root->has_lhs() || root->has_rhs()) {
        std::string new_prefix = prefix + (is_left ? "│   " : "    ");
        if (root->has_lhs()) {
            print_tree(root->get_lhs(), new_prefix, true);
        } else {
            std::cout << new_prefix << "├── [null]" << std::endl;
        }
        if (root->has_rhs()) {
            print_tree(root->get_rhs(), new_prefix, false);
        } else {
            std::cout << new_prefix << "└── [null]" << std::endl;
        }
    }
}

/**
 * live interpreter entry point
 */
int main(int argc, char const *argv[])
{
    std::string inp;
    std::getline(std::cin, inp);

    formula_parser parser = formula_parser();

    node* formula = parser.parse(inp);

    print_tree(formula);

    return 0;
}
