
#include "parser.hpp"
#include "operation.hpp"

#include <exception>
#include <iostream>

using namespace dicey;

void print_tree2(node* root, const std::string& prefix = "", bool is_left = true) {
    if (root == nullptr) {
        return;
    }
    
    std::cout << prefix;
    std::cout << (is_left ? "├── " : "└── ");
    std::cout << root->get_value() << std::endl;

    if (root->has_lhs() || root->has_rhs()) {
        std::string new_prefix = prefix + (is_left ? "│   " : "    ");
        if (root->has_lhs()) {
            print_tree2(root->get_lhs(), new_prefix, true);
        } else {
            std::cout << new_prefix << "├── [null]" << std::endl;
        }
        if (root->has_rhs()) {
            print_tree2(root->get_rhs(), new_prefix, false);
        } else {
            std::cout << new_prefix << "└── [null]" << std::endl;
        }
    }
}

formula_parser::formula_parser() {
	this->options = formula_parser_options::ALL;
}

formula_parser::formula_parser(formula_parser_options options) {
	this->options = options;
}

node* formula_parser::parse(std::string formula) {
	std::size_t cursor;
	std::string buffer;
	active = nullptr;
	root = nullptr;
	state = INITIAL;

	for (cursor = 0; cursor <= formula.size(); cursor++) {
		char current;
		if (cursor >= formula.size()) {
			current = '\0';
		}
		else {
			current = formula[cursor];
		}

		std::cout << "parsing: " << current << " ; " << state << std::endl;

		switch (state) {
		case INITIAL:
			if (current >= '0' && current <= '9') {
				buffer += current;
				state = NUMBER;
			}
			else if (current == ')') {
				buffer.clear();
				while (active->get_parent() != nullptr) {
					if (active->get_value() == "(") {
						break;
					}
					active = active->get_parent();
				}
				state = INITIAL;
			}
			else if (current == '\0') {
				break;
			}
			else {
				throw std::exception();
			}
			break;
		case NUMBER:
			if (current >= '0' && current <= '9') {
				buffer += current;
			}
			else if (current == ')') {
				this->make_number_node(buffer);
				buffer.clear();
				while (active->get_parent() != nullptr) {
					if (active->get_value() == "(") {
						break;
					}
					active = active->get_parent();
				}
				state = INITIAL;
			}
			else if (current == '+' ||
					 current == '-' ||
					 current == '*' ||
					 current == '/' ||
					 (current >= 'a' &&
					 current <= 'z')) {
				this->make_number_node(buffer);
				buffer.clear();
				buffer = current;
				state = OPERATION;
			}
			else if (current == '(') {
				this->make_number_node(buffer);
				buffer.clear();
				this->make_parenthesis_node();
			}
			else if (current == ')') {
				buffer.clear();
				while (active->get_parent() != nullptr) {
					if (active->get_value() == "(") {
						break;
					}
					active = active->get_parent();
				}
				state = INITIAL;
			}
			else if (current = '\0') {
				this->make_number_node(buffer);
			}
			else {
				throw std::exception();
			}
			break;
		case OPERATION:
			if (current >= '0' && current <= '9') {
				this->make_operation_node(buffer);
				buffer.clear();
				buffer = current;
				state = NUMBER;
			}
			else if (current == '+' ||
					 current == '-' ||
					 current == '*' ||
					 current == '/' ||
					 (current >= 'a' &&
					 current <= 'z')) {
				buffer += current;
			}
			else if (current == '(') {
				state = INITIAL;
				this->make_operation_node(buffer);
				buffer.clear();
				this->make_parenthesis_node();
			}
			else {
				throw std::exception();
			}
			break;
		}
		print_tree2(root);
		if (active != nullptr) {
			std::cout << "active: " << active->get_value() << std::endl;
		}
		std::cout << "buffer:  " << buffer << '\n' << std::endl;
	}

	active = nullptr;

	return root;
}

void formula_parser::make_number_node(std::string value) {
	if (active == nullptr) {
		root = new node(value, node_type::VALUE);
		active = root;
		return;
	}

	switch (active->get_type())
	{
	case node_type::OPERATION:
		if (!active->has_lhs()) {
			active->set_lhs(new node(value, node_type::VALUE));
		}
		else {
			active->set_rhs(new node(value, node_type::VALUE));
		}
		break;
	case node_type::PAREN:
		if (!active->has_lhs()) {
			active->set_lhs(new node(value, node_type::VALUE));
			active = active->get_lhs();
		}
		else {
			throw std::exception();
		}
		break;
	default:
		throw std::exception();
		break;
	}
}

void formula_parser::make_operation_node(std::string value) {
	if (active == nullptr) {
		if (value == "-") {
			root = new node(value, node_type::OPERATION);
			root->set_lhs(new node("0", node_type::VALUE));
			active = root;
			return;
		}
		throw std::exception();
	}

	switch (active->get_type())
	{
	case node_type::VALUE: {
		node* shift = new node(value, node_type::OPERATION);
		if (active == root) {
			root = shift;
		}
		shift->set_lhs(active);
		active = shift;
		break;
	}
	case node_type::OPERATION: {
		if (get_operation_priority(value) >= get_operation_priority(active->get_value())) {
			node* shift = new node(value, node_type::OPERATION);
			shift->set_lhs(active->get_rhs());
			active->set_rhs(shift);
			active = shift;
		}
		else {
			node* shift = new node(value, node_type::OPERATION);
			if (active == root) {
				root = shift;
			}
			else {
				active->get_parent()->swap_child(active, shift);
			}
			shift->set_lhs(active);
			active = shift;
		}
		break;
	}
	case node_type::PAREN: {
		if (!active->has_lhs()) {
			node* shift = new node(value, node_type::OPERATION);
			if (active == root) {
				root = shift;
			}
			else {
				active->get_parent()->swap_child(active, shift);
			}
			shift->set_lhs(active);
			active = shift;
		}
		else {
			active->set_rhs(new node(value, node_type::OPERATION));
			active = active->get_rhs();
		}
		break;
	}
	default:
		throw std::exception();
		break;
	}
}

void formula_parser::make_parenthesis_node() {
	if (active == nullptr) {
		root = new node("(", node_type::PAREN);
		active = root;
		return;
	}

	switch (active->get_type())
	{
	case node_type::VALUE: {
		node* shift = new node("*", node_type::OPERATION);
		shift->set_lhs(active);
		active = shift;
		active->set_rhs(new node("(", node_type::PAREN));
		active = active->get_rhs();
		break;
	}
	case node_type::OPERATION: {
		if (!active->has_rhs()) {
			active->set_rhs(new node("(", node_type::OPERATION));
			active = active->get_rhs();
		}
		else {
			node* shift = new node("*", node_type::OPERATION);
			shift->set_lhs(active->get_rhs());
			active->set_rhs(shift);
			active = shift;
		}
		break;
	}
	case node_type::PAREN: {
		if (!active->has_lhs()) {
			active->set_lhs(new node("(", node_type::OPERATION));
			active = active->get_lhs();
		}
		else {
			node* shift = new node("*", node_type::OPERATION);
		}
		break;
	}
	default:
		throw std::exception();
		break;
	}
}