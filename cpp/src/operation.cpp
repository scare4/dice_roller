
#include "operation.hpp"

using namespace dicey;

std::map<std::string, unsigned int> operation_priority{
	{"d", 10},
	{"*", 7},
	{"/", 7},
	{"+", 5},
	{"-", 5},
};

unsigned int dicey::get_operation_priority(std::string op) {
    return operation_priority[op];
}