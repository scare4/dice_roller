
#include "node.hpp"

using namespace dicey;

node::node():
parent(nullptr), lhs(nullptr), rhs(nullptr) {
}

node::node(std::string value, node_type type):
parent(nullptr) {
	this->parent = nullptr;
	this->value = value;
	this->type = type;
}

node::node(const node &rhs):
value(rhs.value), lhs(nullptr), rhs(nullptr) {
	if (rhs.lhs != nullptr) {
		this->lhs = new node(*(rhs.lhs));
	}
	if (rhs.rhs) {
		this->rhs = new node(*(rhs.rhs));
	}
	this->value = rhs.value;
}

node::node(node &&rhs) noexcept:
value(rhs.value), lhs(rhs.lhs), rhs(rhs.rhs)
{
	rhs.lhs = nullptr;
	rhs.rhs = nullptr;
}

node &node::operator=(const node &rhs)
{
	if (this == &rhs) {
		return *this;
	}

	value = rhs.value;

	node* new_lhs = nullptr;
	if (rhs.lhs) {
		new_lhs = new node(*rhs.lhs);
	}
	delete this->lhs;
	this->lhs = new_lhs;

	node* new_rhs = nullptr;
	if (rhs.rhs) {
		new_rhs = new node(*rhs.rhs);
	}
	delete this->rhs;
	this->rhs = new_rhs;

	return *this;
}

node &node::operator=(node &&rhs) noexcept
{
	if (this == &rhs) {
		return *this;
	}

	delete this->lhs;
	delete this->rhs;

	this->value = rhs.value;
	this->lhs = rhs.lhs;
	this->rhs = rhs.rhs;

	rhs.lhs = nullptr;
	rhs.rhs = nullptr;
	return *this;
}

node::~node() {
	delete lhs;
	delete rhs;
}

bool node::has_lhs() {
	return lhs != nullptr;
}

bool node::has_rhs() {
	return rhs != nullptr;
}

node* node::get_lhs() {
	return lhs;
}

node* node::get_rhs() {
	return rhs;
}

node* node::get_parent() {
	return parent;
}

std::string node::get_value() {
	return value;
}

node_type node::get_type() {
	return type;
}

void node::set_lhs(node* lhs) {
	this->lhs = lhs;
	this->lhs->set_parent(this);
}

void node::set_rhs(node* rhs) {
	this->rhs = rhs;
	this->rhs->set_parent(this);
}

node* node::swap_child(node* old_child, node* new_child) {
	if (lhs == old_child) {
		this->set_lhs(new_child);
	}
	else if (rhs == old_child) {
		this->set_rhs(new_child);
	}
	else {
		return nullptr;
	}

	return old_child;
}

void node::set_parent(node* parent) {
	this->parent = parent;
}