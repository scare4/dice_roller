#ifndef NODE_HPP__
#define NODE_HPP__

#include <memory>
#include <string>

#include "node_types.hpp"

namespace dicey {

	class node
	{
	public:
		node();
		node(std::string value, node_type type);
		node(const node& rhs);
		node(node&& rhs) noexcept;
		node& operator=(const node& rhs);
		node& operator=(node&& rhs) noexcept;

		~node();

		bool has_lhs();
		bool has_rhs();
		node* get_lhs();
		node* get_rhs();
		node* get_parent();
		std::string get_value();
		node_type get_type();

		void set_lhs(node* lhs);
		void set_rhs(node* lhs);

		node* swap_child(node* old_child, node* new_child);

	protected:
		void set_parent(node* parent);

	private:
		node* lhs;
		node* rhs;
		node* parent;

		std::string value;
		node_type type;
	};
}

#endif