#ifndef NODE_TYPES_HPP__
#define NODE_TYPES_HPP__

namespace dicey {
	enum class node_type {
		NONE = 0, // invalid default to catch errors
		OPERATION,
		VALUE,
		PAREN,
	};
}

#endif