#ifndef PARSER_HPP__
#define PARSER_HPP__

#include <string>
#include <cstdint>

#include "node.hpp"

namespace dicey {
	enum formula_parser_options: std::uint32_t {
		NONE = 0b0000'0000'0000'0000'0000'0000'0000'0000,
		ALL  = 0b1111'1111'1111'1111'1111'1111'1111'1111,
	};

	class formula_parser {

		public:
			formula_parser();
			formula_parser(formula_parser_options options);

			node* parse(std::string formula);

			// TODO: add way to create and access warnings

		private:
			void make_number_node(std::string value);
			void make_operation_node(std::string value);
			void make_parenthesis_node();

			formula_parser_options options;
			node* active;
			node* root;

			enum state_e {
				INITIAL,
				NUMBER,
				OPERATION,
				PARENT_OPEN,
				PARENT_CLOSE
			};

			state_e state;
	};
}

#endif