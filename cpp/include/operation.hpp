#ifndef OPERATION_HPP_
#define OPERATION_HPP_

#include <map>
#include <string>

namespace dicey {
	unsigned int get_operation_priority(std::string op);
}

#endif