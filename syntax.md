# Constants

 - number : series of digits with no separation
 - boolean : `true` or `false`
 - tuple : comma separated list of formulas
 - text : `"[text]"`
 - comment : any whitespace is ignored, any thing bracketed by `'` is ignored
 - formula : any series of operation

# Operations

## Order of operation

left to right

## Dices

returns a tuple

 - `d[num]` : rolls a single die
 - `[num]d[num]` : rolls a custom number of dices

## Arithmetic

returns a number

 - `[num]+[num]` : addition
 - `[num]-[num]` : substraction
 - `[num]*[num]` : multiplication
 - `[num]/[num]` : division

## Comparisons

returns a boolean

 - `[num]>[num]` : greater
 - `[num]!<[num]` : greater or equal (not lesser)
 - `[num]<[num]` : lesser
 - `[num]!>[num]` : lesser or equal (not greater)
 - `[num]=[num]` : equal
 - `[num]!=[num]` : not equal

## Logic

returns a boolean

 - `![bool]` : not (or invert the next operation)
 - `[bool]&[bool]` : and
 - `[bool]!&[bool]` : nand
 - `[bool]|[bool]` : or
 - `[bool]!|[bool]` : xor

## Selection

returns a tuple

 - `[tuple]^[num]` : selects the **n** highest numbers from a tuple
 - `[tuple]v[num]` : selects the **n** lowest numbers from a tuple

if the number selected is higher than the number of elements in the tuple, all members are selected

## Conditionals

 - `[bool]?[formula];[formula]` : if **false**, returns the first formula, returns the second otherwise
 - `[num]?[formula];[...];[formula]` : returns the **n**th formula, if there are not enough formulas, the last one is treated as default
 - `[num]?[formula]:[num];[...];[formula]` : returns the formula with the number that corresponds to that of the first number, if none corresponds, return the unlabled formula at the end

## Functions

functions without arguments can be treated as variables

 - `([formula]):[text]` : stores the formula in a function
 - `$[text]` : call a function
 - `([formula]);[text];[...];[text]:[text]` : creates a function with arguments
 - `@[text]` : returns the formula held in an argument
 - `$[text];[formula];[...];[formula]` : call a function with arguments

## Output

outputs the result of a specific part of the formula

 - `{[formula]}` : ouputs the result of a formula
 - `{[formula]:[text]}` : outputs the result of a formula with a label
